﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace DotNetLibrary
{
    public class SimpleWeb
    {
        /// <summary>
        /// Get a string from the web
        /// </summary>
        /// <param name="remotePath">The url of the page where to get the string from</param>
        /// <returns>Response from the website of the url</returns>
        public string GetStringFromWeb(string remotePath)
        {
            try
            {
                HttpWebRequest request;
                HttpWebResponse response;
                StringBuilder respBody = new StringBuilder();

                request = (HttpWebRequest)WebRequest.Create(remotePath);
                response = (HttpWebResponse)request.GetResponse();

                byte[] buffer = new byte[1024000000];

                Stream respStream = response.GetResponseStream();

                int count = 0;

                do
                {
                    count = respStream.Read(buffer, 0, buffer.Length);

                    if (count != 0)
                        respBody.Append(Encoding.ASCII.GetString(buffer, 0, count));
                } while (count > 0);


                return respBody.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// Get the size of a file
        /// </summary>
        /// <param name="remotePath">The url of the file</param>
        /// <returns>The file size in bytes</returns>
        public long GetFileSize(string remotePath)
        {
            try
            {
                HttpWebRequest request;
                HttpWebResponse response;

                request = (HttpWebRequest)WebRequest.Create(remotePath);
                request.Method = "HEAD";

                using (response = (HttpWebResponse)request.GetResponse())
                {
                    long contentlength;

                    if (long.TryParse(response.Headers.Get("Content-Length"), out contentlength))
                        return contentlength;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }
}
