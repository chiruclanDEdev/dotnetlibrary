﻿/* 
 * == DotNetLibrary from Chiruclan - Simple steps for learning C# ==
 * Copyright (C) 2013  Chiruclan
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DotNetLibrary
{
    public class SimpleLogging
    {
        private readonly string _logFileName;

        /// <summary>
        /// Initialize logging
        /// </summary>
        /// <param name="logFileName">File to log to</param>
        public SimpleLogging(string logFileName)
        {
            _logFileName = logFileName;
        }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="LogMessage">Message to log</param>
        /// <param name="useTimestamp">Decide to use a timestamp or not</param>
        public void Log(string LogMessage, bool useTimestamp = false)
        {
            using (FileStream LogFile = new FileStream(_logFileName, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter LogStream = new StreamWriter(LogFile))
                {
                    if (useTimestamp)
                        LogMessage = "[" + DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00") + "] " + LogMessage;

                    LogStream.WriteLine(LogMessage);
                    LogStream.Flush();
                }
            }
        }
    }
}
