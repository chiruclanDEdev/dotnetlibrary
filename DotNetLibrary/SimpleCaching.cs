﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLibrary
{
    public class SimpleCaching
    {
        private Dictionary<Object, Object> Cache = new Dictionary<Object, Object>();

        /// <summary>
        /// Initialize cache
        /// </summary>
        public SimpleCaching()
        {
            return;
        }

        /// <summary>
        /// Set the value of an item or remove it
        /// </summary>
        /// <param name="Item">The item to set or delte</param>
        /// <param name="Value">The value of the item</param>
        public void SetItem(object Item, object Value = null)
        {
            if (!Cache.ContainsKey(Item) & Value != null)
                Cache.Add(Item, Value);
            else if (Cache.ContainsKey(Item) & Value != null)
                Cache[Item] = Value;
            else if (Cache.ContainsKey(Item) & Value == null)
                Cache.Remove(Item);
        }

        /// <summary>
        /// Get the value of an item
        /// </summary>
        /// <param name="Item">The item which value you want to have</param>
        /// <returns>The value of the item</returns>
        public object GetItem(object Item)
        {
            if (Cache.ContainsKey(Item))
                return Cache[Item];

            return null;
        }

        /// <summary>
        /// Get all items
        /// </summary>
        /// <returns>A list with all items</returns>
        public List<Object> GetItemList()
        {
            List<Object> ItemList = new List<Object>();

            foreach (object Item in Cache.Keys)
                ItemList.Add(Item);

            return ItemList;
        }

        /// <summary>
        /// Get all items with it's values
        /// </summary>
        /// <returns>A dictionary with all items and it's values</returns>
        public Dictionary<Object, Object> GetAllItems()
        {
            return Cache;
        }

        /// <summary>
        /// Check if an item exists
        /// </summary>
        /// <param name="Item">The item to check</param>
        /// <returns>Returns true or false</returns>
        public bool ItemExists(object Item)
        {
            if (Cache.ContainsKey(Item))
                return true;

            return false;
        }

        /// <summary>
        /// Delete all items from the cache
        /// </summary>
        public void ClearCache()
        {
            Cache.Clear();
        }

        /// <summary>
        /// Insert content into cache
        /// </summary>
        /// <param name="Content">An object dictionary with content to insert</param>
        public void InsertCache(Dictionary<Object, Object> Content)
        {
            foreach (object Obj in Content.Keys)
            {
                if (!Cache.ContainsKey(Obj))
                    Cache.Add(Obj, Content[Obj]);
                else
                    Cache[Obj] = Content[Obj];
            }
        }

        /// <summary>
        /// Load content into cache
        /// </summary>
        /// <param name="Content">An object dictionary with content to load</param>
        public void LoadCache(Dictionary<Object, Object> Content)
        {
            Cache = Content;
        }
    }
}
