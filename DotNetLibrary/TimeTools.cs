﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLibrary
{
    public class TimeTools
    {
        public long getUnixTimestamp()
        {
            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)t.TotalSeconds;
        }

        public DateTime convertUnixTimestamp(long unixTimestamp = -1)
        {
            if (unixTimestamp == -1)
                unixTimestamp = getUnixTimestamp();

            DateTime d = new DateTime(1970, 1, 1, 0, 0, 0);
            d = d.AddSeconds(unixTimestamp);

            return d;
        }

        public struct convertTimestamp
        {
            private long Dif;
            public long Days, Hours, Minutes, Seconds;

            public convertTimestamp(long timestamp)
            {
                Dif = timestamp;
                Days = 0;
                Hours = 0;
                Minutes = 0;
                Seconds = 0;

                if (Dif >= 86400)
                {
                    Days = (long)Math.Floor((double)Dif / 86400);
                    Dif = Dif - (Days * 86400);
                }

                if (Dif >= 3600)
                {
                    Hours = (long)Math.Floor((double)Dif / 3600);
                    Dif = Dif - (Hours * 3600);
                }

                if (Dif >= 60)
                {
                    Minutes = (long)Math.Floor((double)Dif / 60);
                    Dif = Dif - (Minutes * 60);
                }

                Seconds = Dif;
            }
        }
    }
}
