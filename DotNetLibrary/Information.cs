﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLibrary
{
    public class Information
    {
        public Version Version
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version; }
        }
    }
}
