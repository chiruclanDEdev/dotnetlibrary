﻿/* 
 * == DotNetLibrary from Chiruclan - Simple steps for learning C# ==
 * Copyright (C) 2013  Chiruclan
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DotNetLibrary
{
    /// <summary>
    /// A simple class for using configuration files
    /// </summary>
    public class SimpleConfiguration
    {
        private readonly string _configFileName;
        private readonly bool _readOnly;

        private SortedDictionary<string, string> KeyList = new SortedDictionary<string, string>();
        private SortedDictionary<string, string> CommentList = new SortedDictionary<string, string>();

        public SimpleConfiguration(string configFileName, bool readOnly = false)
        {
            _configFileName = configFileName;
            _readOnly = readOnly;
            Read();
        }

        /// <summary>
        /// Gets the data from the configuration file
        /// </summary>
        public void Read()
        {
            try
            {
                KeyList.Clear();

                using (FileStream ConfigFile = new FileStream(_configFileName, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    using (StreamReader ConfigReader = new StreamReader(ConfigFile))
                    {
                        string[] Current = null;
                        string CurrentLine = null;
                        string CurrentKey = null;
                        string CurrentValue = null;
                        string CurrentComment = "";

                        do
                        {
                            CurrentLine = ConfigReader.ReadLine();

                            if (!CurrentLine.StartsWith("#") & !string.IsNullOrEmpty(CurrentLine) & !string.IsNullOrWhiteSpace(CurrentLine) & CurrentLine.Contains("="))
                            {
                                Current = CurrentLine.Split(new char[] { '=' }, 2);

                                CurrentKey = Current[0].Trim();
                                CurrentValue = Current[1].Trim();

                                KeyList.Add(CurrentKey, CurrentValue);

                                if (!string.IsNullOrEmpty(CurrentComment))
                                {
                                    CommentList.Add(CurrentKey, CurrentComment);
                                    CurrentComment = "";
                                }
                            }
                            else if (CurrentLine.StartsWith("#"))
                            {
                                if (string.IsNullOrEmpty(CurrentComment))
                                    CurrentComment = CurrentLine.Substring(1).Trim();
                                else
                                    CurrentComment += Environment.NewLine + CurrentLine.Substring(1).Trim();
                            }

                            if (string.IsNullOrEmpty(CurrentLine) | string.IsNullOrWhiteSpace(CurrentLine))
                                CurrentLine = "#";
                        } while (!(CurrentLine == null));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Saves the data to the configuration file
        /// </summary>
        public void Save()
        {
            if (_readOnly)
                return;

            try
            {
                using (FileStream ConfigFile = new FileStream(_configFileName, FileMode.Create))
                {
                    using (StreamWriter ConfigWriter = new StreamWriter(ConfigFile))
                    {
                        string PreviousMainKey = "";
                        string CurrentMainKey = "";

                        foreach (string Key in KeyList.Keys)
                        {
                            if (Key.Contains("."))
                                CurrentMainKey = Key.Split(new string[] { "." }, 2, StringSplitOptions.None)[0];
                            else
                                CurrentMainKey = Key.Substring(0, 1);

                            if (!(CurrentMainKey == PreviousMainKey))
                            {
                                if (!string.IsNullOrWhiteSpace(PreviousMainKey) & !string.IsNullOrEmpty(PreviousMainKey) & !string.IsNullOrWhiteSpace(CurrentMainKey) & !string.IsNullOrEmpty(CurrentMainKey))
                                    ConfigWriter.WriteLine();

                                PreviousMainKey = CurrentMainKey;
                            }

                            if (CommentExists(Key))
                            {
                                foreach (string Comment in GetComment(Key).Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
                                {
                                    ConfigWriter.WriteLine("# " + Comment.Trim());
                                }
                            }

                            ConfigWriter.WriteLine(Key + " = " + KeyList[Key]);
                        }

                        ConfigWriter.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Clears the while configuration data
        /// </summary>
        public void Clear()
        {
            if (_readOnly)
                return;

            KeyList.Clear();
            CommentList.Clear();
        }

        /// <summary>
        /// Checks wether a key exists or not
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool KeyExists(string Key)
        {
            if (KeyList.ContainsKey(Key))
                return true;

            return false;
        }

        /// <summary>
        /// Lists all existing keys
        /// </summary>
        /// <returns></returns>
        public List<string> ListKeys()
        {
            List<String> Keys = new List<String> ();

            foreach (string Key in KeyList.Keys)
                Keys.Add(Key);

            return Keys;
        }

        /// <summary>
        /// Gets the value of a key
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string GetKey(string Key)
        {
            if (KeyList.ContainsKey(Key))
                return KeyList[Key];

            return null;
        }

        /// <summary>
        /// Adds/Sets/Removes a key with value
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public void SetKey(string Key, string Value = null)
        {
            if (string.IsNullOrEmpty(Key) | string.IsNullOrWhiteSpace(Key) | _readOnly)
                return;

            if (!string.IsNullOrEmpty(Value))
            {
                if (KeyList.ContainsKey(Key))
                    KeyList[Key] = Value;
                else
                    KeyList.Add(Key, Value);
            }
            else
            {
                KeyList.Remove(Key);
                CommentList.Remove(Key);
            }
        }

        /// <summary>
        /// Checks whether the given list of keys exist or not
        /// </summary>
        /// <param name="Keys"></param>
        /// <returns></returns>
        public bool CheckKeys(List<String> Keys)
        {
            foreach (string Key in Keys)
            {
                if (!KeyExists(Key))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether a comment exists or not
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool CommentExists(string Key)
        {
            if (CommentList.ContainsKey(Key))
                return true;

            return false;
        }

        /// <summary>
        /// Lists all keys with a comment
        /// </summary>
        /// <returns></returns>
        public List<string> ListComments()
        {
            List<String> Comments = new List<String> ();

            foreach (string Comment in CommentList.Keys)
                Comments.Add(Comment);

            return Comments;
        }

        /// <summary>
        /// Gets the comment of a key
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string GetComment(string Key)
        {
            if (CommentList.ContainsKey(Key))
                return CommentList[Key];

            return null;
        }

        /// <summary>
        /// Sets/removes a comment of a key
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value">Comment (leave it empty to remove it)</param>
        /// <returns></returns>
        public void SetComment(string Key, string Value = null)
        {
            if (string.IsNullOrEmpty(Key) | string.IsNullOrWhiteSpace(Key) | _readOnly)
                return;

            if (!string.IsNullOrEmpty(Value))
            {
                if (CommentList.ContainsKey(Key))
                    CommentList[Key] = Value;
                else
                    CommentList.Add(Key, Value);
            }
            else
                CommentList.Remove(Key);
        }
    }
}
