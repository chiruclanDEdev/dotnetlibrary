﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace DotNetLibrary
{
    public class IOTools
    {
        public void ShellExecute(string command, string arguments = null)
        {
            try
            {
                if (string.IsNullOrEmpty(command) | string.IsNullOrWhiteSpace(command))
                    return;

                Process proc = new Process();

                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = command;
                proc.StartInfo.CreateNoWindow = true;

                if (!string.IsNullOrEmpty(arguments) & !string.IsNullOrWhiteSpace(arguments))
                    proc.StartInfo.Arguments = arguments;

                proc.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public DateTime GetLinkerTimestamp(string applicationPath)
        {
            string filePath = applicationPath;
            const int c_PeHeaderOffSet = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            Stream s = null;

            try
            {
                using (s = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    s.Read(b, 0, 2048);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffSet);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);

            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);

            return dt;
        }
    }
}
