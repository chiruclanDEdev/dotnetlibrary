﻿/* 
 * == DotNetLibrary from Chiruclan - Simple steps for learning C# ==
 * Copyright (C) 2013  Chiruclan
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace DotNetLibrary
{
    public class SimpleCryptography
    {

        /// <summary>
        /// Hash as md5
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string MD5Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            MD5 CryptoService = new MD5CryptoServiceProvider();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as sha1
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string SHA1Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            SHA1 CryptoService = new SHA1CryptoServiceProvider();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as sha256
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string SHA256Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            SHA256 CryptoService = new SHA256CryptoServiceProvider();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as sha384
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string SHA384Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            SHA384 CryptoService = new SHA384CryptoServiceProvider();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as sha512
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string SHA512Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            SHA512 CryptoService = new SHA512CryptoServiceProvider();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as hmac md5
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string HMACMD5Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            HMACMD5 CryptoService = new HMACMD5();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as hmac sha1
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string HMACSHA1Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            HMACSHA1 CryptoService = new HMACSHA1();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as hmac sha256
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string HMACSHA256Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            HMACSHA256 CryptoService = new HMACSHA256();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as hmac sha384
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string HMACSHA384Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            HMACSHA384 CryptoService = new HMACSHA384();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }

        /// <summary>
        /// Hash as hmac sha512
        /// </summary>
        /// <param name="StringToHash">String to hash</param>
        /// <returns>Hash of the string</returns>
        public string HMACSHA512Hash(string StringToHash)
        {
            if (string.IsNullOrEmpty(StringToHash))
                return string.Empty;

            HMACSHA512 CryptoService = new HMACSHA512();

            byte[] BytesToHash = Encoding.Default.GetBytes(StringToHash);
            byte[] HashResult = CryptoService.ComputeHash(BytesToHash);

            StringBuilder Result = new StringBuilder();

            foreach (byte b in HashResult)
                Result.Append(b.ToString("x2"));

            return Result.ToString();
        }
    }
}
